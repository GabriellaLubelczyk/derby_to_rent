import request from '@/utils/request';
const api = "/api/";

export function pitchList() {
    return request({
      url: 'pitch',
      method: 'get',
    });
  }
  export function pitchList_Rating() {
    return request({
      url: 'pitch/order',
      method: 'get',
    });
  }
export function fetchPitch(id) {
    return request({
      url:  'pitch/' + id,
      method: 'get',
    });
  } 
  export function DeletePitch(id) {
    return request({
      url:  'pitch/' + id,
      method: 'delete',
    });
  } 
export function createPitch(data) {
    return request({
      url: '/pitch',
      method: 'post',
      data,
    });
  }
export function updatePitch(id , data) {
    return request({
      url: '/pitch/'+ id,
      method: 'put',
      data,
    });
  }
  export function acceptPitch(id) {
    return request({
      url: 'pitch/accept/' + id,
      method: 'get',
    });
  }

export function rejectPitch(id) {
    return request({
      url: 'pitch/reject/' + id,
      method: 'get',
    });
  } 
  export function TopPitchList() {
    return request({
      url: 'pitch/ranking/top10',
      method: 'get',
    });
  }
  export function Mypitch(id) {
    return request({
      url:  'pitch/landlord/' + id,
      method: 'get',
    });
  } 