import request from '@/utils/request';

export function ReviewList() {
    return request({
      url: 'review',
      method: 'get',
    });
  }
  export function MyReview(id) {
    return request({
      url: 'review/user/' + id ,
      method: 'get',
    });
  }
  export function FetchReview(id) {
    return request({
      url: 'review/' + id ,
      method: 'get',
    });
  }