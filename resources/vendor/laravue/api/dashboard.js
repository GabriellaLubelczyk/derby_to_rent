import request from '@/utils/request';

export function DashList() {
  return request({
    url: '/dashboard',
    method: 'get',
  });
}

export function TicketYear() {
  return request({
    url: '/dashboard/ticket/year',
    method: 'get',
  });
}

export function PurchaseYear() {
  return request({
    url: '/dashboard/ticket/year/money',
    method: 'get',
  });
}