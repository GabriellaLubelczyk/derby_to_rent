import request from '@/utils/request';
const api = "/api/";

export function EventList() {
    return request({
      url: 'event',
      method: 'get',
    });
  }
export function fetchEvent(id) {
    return request({
      url: 'event/' + id,
      method: 'get',
    });
  } 
  export function deleteEvent(id) {
    return request({
      url:  'event/' + id,
      method: 'DELETE',
    });
  } 
export function createEvent(data) {
    return request({
      url: '/event',
      method: 'post',
      data,
    });
  }
  export function updateEvent(id , data) {
    return request({
      url: '/event/'+ id,
      method: 'put',
      data,
    });
  }
  export function Calendar() {
    return request({
      url: '/event/calendar/days',
      method: 'get',
    });
  }