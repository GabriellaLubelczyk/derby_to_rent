import request from '@/utils/request';

export function TopUser() {
  return request({
    url: '/user/ranking/top',
    method: 'get',
  });
}
