import Layout from '@/views/layout/Layout';

const UserRoute = {
    path: '/user',
    component: Layout,
    meta: { title: 'User Stuffs', icon: 'money', noCache: true },
    children: [
      {
        path: 'ticket',
        name: 'MyTickets',
        component: require('@/views/servio/user/myticket').default,
        meta: { title: 'MyTickets', icon: 'shopping' },
      },
      {
        path: 'ticket/old',
        name: 'OldTickets',
        component: require('@/views/servio/user/myoldticket').default,
        meta: { title: 'OldTickets', icon: 'shopping' },
      },
      {
        path: 'review',
        name: 'MyReview',
        component: require('@/views/servio/user/myreview').default,
        meta: { title: 'MyReview', icon: 'shopping' },
      },
      {
        path: 'editreview/:id',
        name: 'editReview',
        hidden: true,
        component: require('@/views/servio/user/editmyreview').default,
        meta: { title:'editReview' , hidden: true },
        params: true,
      },
    ]
};

export default UserRoute;
