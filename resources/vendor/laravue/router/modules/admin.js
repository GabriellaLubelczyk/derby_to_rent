import Layout from '@/views/layout/Layout';

const adminroute = {
    path: '/admin',
    component: Layout,
    meta: { title: 'Administration', icon: 'money', noCache: true },
    children: [
      {
        path: 'ticket',
        name: 'Tickets',
        component: require('@/views/servio/TicketIndex').default,
        meta: { title: 'Tickets', icon: 'shopping', breadcrumb: false },
      },
      {
        path: 'pitch',
        name: 'Pitches',
        component: require('@/views/servio/C3Tindex').default,
        meta: { title: 'Pitches', icon: 'shopping' },
      },
      {
        path: 'pitch/add',
        name: 'AddPitch',
        hidden: true,
        component: require('@/views/servio/index2').default,
        meta: { title: 'Create Pitch',  hidden: true },
      },
      {
        path: 'event',
        name: 'Events',
        component: require('@/views/servio/EventIndex').default,
        meta: { title: 'Events', icon: 'shopping' },
      },
      {
        path: 'event/add',
        name: 'AddEvent',
        hidden: true,
        component: require('@/views/servio/AddEvent').default,
        meta: { title: 'AddEvent', hidden: true, },
      },
      {
        path: 'editpitch/:id',
        name: 'editPitch',
        hidden: true,
        component: require('@/views/servio/index3').default,
        meta: { title:'editPitch' , hidden: true },
        params: true,
      },
      {
        path: 'editevent/:id',
        name: 'editEvent',
        hidden: true,
        component: require('@/views/servio/EditEvent').default,
        meta: { title:'editEvent' , hidden: true },
        params: true,
      },
    ],
  };

  export default adminroute;