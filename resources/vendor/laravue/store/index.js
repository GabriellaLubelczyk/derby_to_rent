import Vue from 'vue';
import Vuex from 'vuex';
import app from './modules/app';
import permission from './modules/permission';
import tagsView from './modules/tags-view';
import user from './modules/user';
import getters from './getters';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    app,
    user,
    permission,
    tagsView,
  },
  state: {
    pitches: [{
      id: 1 ,
      name: 'FIRST',
      price_h: 400,
      address: 'Rugby-ave',
      number: 1243654,
  },
  {
      id: 2 ,
      name: 'SECOND',
      price_h: 300,
      address: 'Crownwell-road',
      number: 12354454,

  }],
  columns: [
    {
      text: 'Pitch',
      value: 'name',
      width: 200,
    },
    {
      text: 'ID',
      value: 'id',
    },
    {
      text: 'Address',
      value: 'address',
    },
    {
      text: 'Number',
      value: 'number',
    },
    {
      text: 'Price',
      value: 'price_h',
    },
    {
      text: 'Action',
      value: 'price_h',
    },
  ],
  event: [
    {
      id: 0,
      event: 'Event 1',
      comment: 'yes',
      date: '12/05/2019',
    },
    {
      id: 1,
      event: 'Event 2',
      comment: 'No',
      date: '22/05/2019',
},
],
tickets: [
  {
    id: 0,
    account: '1',
    footballpitch: '1',
    date: '12/05/2019',
  },
  {
    id: 1,
    account: '2 ',
    footballpitch: '1',
    date: '22/05/2019',
},
],
  },
  getters,
  mutations: {
    addPitch: (state, pitch) => {
      state.pitches.push(pitch)
    },
    dltPitch: (state, pitch) => {
      state.pitches.splice(pitch , 1)
    },
    edtPitch: (state, pitch, id_pitch) => {
      Vue.set(state.pitches, id_pitch, pitch)
    }
  }, 
  actions: {
    rmvPitch: (context, pitch) => {
      context.commit('dltPitch', pitch)
    }
  }
});

export default store;
