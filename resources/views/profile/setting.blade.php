@extends('base')
<html>
    <head>
        <h1> Show all yours tickets </h1>

    </head>
    <div class="col-sm-12">
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">User</h1>    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>Name</td>
          <td>Surname</td>
          <td>Phone NUmber</td>
          <td>Email</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>  <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{$user->name}}</td>
            <td>{{$user->surname}}</td>
            <td>{{$user->phone_number}}</td>
            <td>{{$user->email}}</td>
        </tr>
        @endforeach
    </tbody>  
  </table>

</div>
</html>
