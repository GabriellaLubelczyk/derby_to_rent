<html>
@extends('layouts.head')

    @include('layouts.header')
<body>
    <div class="container">
        <div class="row">
             <div class="container">
                 <!--Review like a chat-->
                 @foreach($footballpitch as $foot)
                    @if($foot->id === $pitch->footballpitch)
                    <h1 class="review-title">Review {{$foot->name}}</h1>
                  
                <div class="card-reviews" style="background: url({{$foot->image}});background-repeat: no-repeat;">
                        @endif
                        @endforeach
                    @foreach($reviews as $revie)
                        @if($revie->account == $user->id)
                            <div class=" card-your-review ">
                                <image class="avatar-chat" src="{{$user->image}}"></image>
                                <h3>You</h3>
                                <div>
                                    @for($i=0; $i < $revie->star; $i++)
                                    <span class="fa fa-star checked"></span>
                                    @endfor
                                    @for($i=5; $i > $revie->star; $i --)
                                    <span class="fa fa-star"></span>
                                    @endfor
                                </div>
                                <p>{{$revie->comment}}</p>

                                <p><small><i class="fa fa-check" aria-hidden="true"></i>{{$revie->created_at}}</small></p>
                            </div>
                        @else
                            <div class="card card-somebody-review">
                                @foreach($users as $boi)
                                    @if($boi->id === $revie->account)
                                    <image class="avatar-chat" src="{{$boi->image}}"></image>
                                    <h3>{{$boi->name}} {{$boi->surname}}</h3>
                                    @endif
                                @endforeach
                                <div>
                                        @for($i=0; $i<$revie->star; $i++)
                                        <span class="fa fa-star checked"></span>
                                        @endfor
                                        @for($i=5; $i>$revie->star; $i --)
                                        <span class="fa fa-star"></span>
                                        @endfor
                                </div>
                                    <p>{{$revie->comment}}</p>
                                <p><small><i class="fa fa-check" aria-hidden="true"></i>{{$revie->created_at}}</small></p>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="card write-comment-card">
                <form action="{{route('review.store')}}" method="post" >
                    @csrf
                    
                    <div class="form-group">
                            <label>Your review:</label>
                            <input type="text" class="form-control" name="comment" placeholder="write here ..."/>
                    </div>

                    <div class="form-group">
                        <input type="hidden" class="form-control" name="account" value="{{$user->id}}"/>
                    </div>

                    <div class="form-group">
                        <input type="hidden" class="form-control" name="ticket" value="{{$id}}"/>
                    </div>

                    <div class="form-group">
                        <input type="hidden" class="form-control" name="footballpitch" value="{{$pitch->footballpitch}}">
                    </div>

                    <div class="rate">
                            <input type="radio" id="star5" name="star" value="5" />
                            <label for="star5" title="5">5 stars</label>
                            <input type="radio" id="star4" name="star" value="4" />
                            <label for="star4" title="4">4 stars</label>
                            <input type="radio" id="star3" name="star" value="3" />
                            <label for="star3" title="3">3 stars</label>
                            <input type="radio" id="star2" name="star" value="2" />
                            <label for="star2" title="2">2 stars</label>
                            <input type="radio" id="star1" name="star" value="1" />
                            <label for="star1" title="1">1 star</label>
                    </div>
                        <button  type="submit" class="btn btn-primary" >Send</button>
                </form>
        </div>
        </div>
    </div>
</body>
@include('layouts.footer')
</html>