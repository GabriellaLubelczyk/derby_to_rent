<html>
@extends('layouts.head')

    @include('layouts.header')
   <body>     
    @section('main')
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <div class="card card-user">
                    <h1> Your Reviews:</h1>
                   <h3>Star: {{$review->star}}</h3>
                   <h3>Review:{{$review->review}}</h3>
                <!--   <h3>Account: {{$review->account}}</h3>
                   <h3>Footballpitch {{$review->footballpitch}}</h3>  -->
                    
            </div>
                </div>
                @can('Create a pitch')
                <div class="col-6">
                        
                    <a href="{{route('review.create')}}">Create new review</a>
                       
                </div>
                @endcan
            </div>
            <div class="container">
                <div class="row">
                    <div>
                        
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footer')
    </body>
    </html>