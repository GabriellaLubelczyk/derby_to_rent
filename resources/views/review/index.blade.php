@extends('base')
@section('main')
<html>
    <head>
        <h1> Show all yours tickets </h1>
        <a style="margin: 19px;" href="{{ route('review.create')}}" class="btn btn-primary">Return</a>

    </head>
    <div>
    <a style="margin: 19px;" href="{{ route('review.create')}}" class="btn btn-primary">New review</a>
    </div>  
    <div class="col-sm-12">
@section('main')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Review</h1>    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>Star</td>
          <td>Review</td>
     <!--     <td>Account</td>
          <td>Footballpitch</td> -->
          <td colspan = 2>Actions</td>
        </tr>
    </thead>  <tbody>
        @foreach($review as $review)
        <tr>
            <td>{{$review->star}}</td>
            <td>{{$review->review}}</td>
          <!--  <td>{{$review->account}}</td>
            <td>{{$review->footballpitch}}</td> */ -->
            <td>
                <a href="{{ route('review.edit',$review->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('review.destroy', $review->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>  
  </table>
<div>
</div>
@endsection


  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</html>