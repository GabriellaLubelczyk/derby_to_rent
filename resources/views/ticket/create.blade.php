<html>
@extends('layouts.head')
@include('layouts.header')

<body>
   <!--- <h2> Buy a ticket </h2>
    <form method="POST" action="{{ route('ticket.store') }}">
          @csrf
          <div class="form-group">    
              <label for="date">Date:</label>
              <input type="date" class="form-control" name="date"/>
          </div>

          <div class="form-group">
              <label for="time">Time:</label>
              <input type="time" class="form-control" name="time"/>
          </div>

          <div class="form-group">
              <label for="address">Address:</label>
              <input type="text" class="form-control" name="address"/>
          </div>
          <div class="form-group">
              <label for="import">Import:</label>
              <input type="text" class="form-control" name="import"/>
          </div>        
          <div class="form-group">
              <label for="number">Number of people:</label>
              <input type="integer" class="form-control" name="number"/>
          </div>        
          <div>
    <button style="margin: 19px;" type="submit" class="btn btn-primary" >click</button>
    </div> 
          <div>
    <a style="margin: 19px;" href="{{ route('ticket.index')}}" class="btn btn-primary">View list of your tickets</a>
    </div> 
      </form>-->   
      @role('isUser')
    <div class="container">
        <div class="row">
            <h1 class="buyticket-card-title">Submit ticket request to the Landlord</h1>
            <div class="col-7">
                <div class="padding">
                <p class="intro-card-text">Terms and condition.
                        I Termini e le Condizioni d'Uso possono essere adottati per qualsiasi tipologia di sito web o di applicazione, come e-commerce, blog, siti di annunci, siti informativi e di notizie, forum, servizi online, giochi, etc.
                        Per i siti che svolgono attività di e-commerce di beni e/o servizi, e in particolare se l'utente è un "consumatore" (inteso come la persona fisica che agisce per scopi estranei rispetto all’attività imprenditoriale commerciale, artigianale o professionale eventualmente svolta), è obbligatorio fornire specifiche informazioni che riguardano il diritto di recesso (annullamento dell’acquisto), i termini e le modalità per il suo esercizio, la conclusione della vendita, la consegna, etc. (D. Lgs. 206/2005, Codice del Consumo). La violazione degli obblighi informativi può comportare nei confronti del titolare sanzioni che vanno da € 103 a € 10.000. 
                </p>
            </div>
            </div>
            <div class="col-5 ">
                <div class="card card-remind-pitch">
                    <h3 class="card-remind-pitch-title">{{$pitch->name}}</h3>
                    <p>Price at hour : {{$pitch->price_h}} £</p>
                    <p>City: {{$pitch->city}}</p>
                    <p>Address: {{$pitch->address}}</p>
                    <image class="card-remind-pitch-img" src="{{$pitch->image}}"></image>
                </div>
            </div>
            <div class="col-6">
                <div class="card card-ticket">
                    @php
                    $double = $pitch->price_h *2;
                    @endphp
                    <form action="{{route('ticket.store')}}" method="POST">
                        @csrf
                            <div class="form-group">
                                    <label for="number">Number of people:</label>
                                    <input type="integer" class="form-control" name="number" placeholder="For example 22...."/>
                            </div> 

                            <input type="hidden" class="form-control" name="time" value="{{$date}}"/>
                            <input type="hidden" class="form-control" name="footballpitch" value="{{$pitch->id}}"/>
                            <input type="hidden" class="form-control" name="account" value="{{$user->id}}"/>
                             <div class="form-group">
                                    <label for="date">When:</label>
                                    <input type="date" class="form-control" name="date" value=""/>
                            </div> 
                            <div class="form-group">
                                    <label for="hours">How much time:</label>
                                    <select class="form-control" name="import" id="import">
                                            <option value="{{$pitch->price_h}}">1</option>
                                            <option value="{{$double}}">2</option>
                                        </select>
                            </div> 
                            <button class="btn btn-primary" type="submit">Buy</button>
                    </form>
                </div>
            </div>
            <div class="col-6">
                    <div class="card card-payement-method">
                    <div class="form-container">
                    <div class="field-container">
                        <label for="name">Name</label>
                        <input id="name" maxlength="20" type="text">
                    </div>
                    <div class="field-container">
                        <label for="cardnumber">Card Number</label><span id="generatecard">generate random</span>
                        <input id="cardnumber" type="text" pattern="[0-9]*" inputmode="numeric">
                        <svg id="ccicon" class="ccicon" width="750" height="471" viewBox="0 0 750 471" version="1.1" xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink">

                        </svg>
                    </div>
                    <div class="field-container">
                        <label for="expirationdate">Expiration (mm/yy)</label>
                        <input id="expirationdate" type="text" pattern="[0-9]*" inputmode="numeric">
                    </div>
                    <div class="field-container">
                        <label for="securitycode">Security Code</label>
                        <input id="securitycode" type="text" pattern="[0-9]*" inputmode="numeric">
                    </div>
                </div>
                    </div>
                </div>
        </div>
    </div>
    @endrole
</body>
@include('layouts.footer')
</html>