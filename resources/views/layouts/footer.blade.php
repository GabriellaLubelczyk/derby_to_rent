<footer>
          <div class="foot">
            <div class="container">
                <div class="row">
                    <div class="col-6">
                      <a href="https://www.facebook.com/"><img class="facebook-logo" src="https://image.flaticon.com/icons/png/512/124/124010.png" /></a>
                    </div>
                    <div class="col-6">
                        <div class=" card-contact-us">
                            <h2 class="card-contact-us-title">Contact us</h2>
                            <h5 class="card-contact-us-adress">Email : francescosaverio.cinaglia@gmail.com</h5>
                            <h5 class="card-contact-us-adress">Phone : +39 3922328173</h5>
                            <h5 class="card-contact-us-adress">Address : Via Alcide De Gasperi N.28</h5>
                         </div>
                    </div>
                </div>
            </div>
          </div>
        </footer>