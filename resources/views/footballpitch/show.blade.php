<html>
@extends('layouts.head')

@include('layouts.header')
    <body>
        <div class="page-football-image first-container" style="background: url({{$pitch->image}});">

        </div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                            <div class="card page-football-card margin-top">
                               <h1 class="page-football-card-title"> {{$pitch->name}}</h1>
                               <div class="row">
                                   <div class="col-6">
                                    <h5>Description:{{$pitch->description}}</h5>
                                    <h4>{{$pitch->city}}</h4>
                                    <h4>{{$pitch->address}}</h4>
                                    <h4>{{$pitch->postalcode}}</h4>
                                    @foreach($user as $lind)
                                        @if($lind->id === $pitch->landlord)	
                                            <h5>Landlord: {{$lind->name}} {{$lind->surname}}</h5>
                                        @endif
                                    @endforeach
                                   </div>
                                   @role('isLandlord')
                                    I can't buy tickets
                                    @endcan
                                   
                                   @role ('isUser')
                                   <div class="col-5">
                                   You can buy tickets
                                       <div class="card ticket-card">
                                           <h2 class="ticket-card-title"><a href="{{route('ticket.createticket' , $pitch->id)}}">Buy a ticket<a></h2>
                                            <h4 class="ticket-card-price" >Price:{{$pitch->price_h}} £ at hour</h4>
                                            <h4 class="ticket-card-price">Max-player:{{$pitch->max_player}}</h4>
                                            
                                       </div>
                                    </div>
                                    <div>
                                        
                                    </div>
                                    @endrole
                               </div>   
                               <div class="container margin-top">
                                    <!--The div element for the map -->
                                    <div id="map"></div>
                                    <script>
                                            var la = {{$pitch->lat}};
                                            var lo = {{$pitch->long}};
                                            console.log(lo , la);
                                            // Initialize and add the map
                                            function initMap() {
                                              // The location of Uluru
                                              var uluru = {lat: la, lng: lo };
                                              // The map, centered at Uluru
                                              var map = new google.maps.Map(
                                                  document.getElementById('map'), {zoom: 4, center: uluru});
                                              // The marker, positioned at Uluru
                                              var marker = new google.maps.Marker({position: uluru, map: map});
                                            }
                                        </script>
                               </div>
                               <div class="row margin-top">
                                <div class="col-6">
                                    @foreach($equip as $equipment)
                                        @if($equipment->id === $pitch->equipment)	
                                            <h5 class="football-card-equipment">Equipment: {{$equipment->name}}<small>{{$equipment->description}} </small> </h5>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="col-6">
                                        <img class="card-news-img" src="{{$pitch->image2}}" >
                                </div>
                               </div>
                                 <div class="card-comments">
                                      <form>
                                     <label> <h3>Review:</h3></label>
                                     @role ('isUser')
                                     <input type="text" class="form-group input-text-review" placeholder="Write here.." name="comment"/>
                                     <input type="hidden">
                                     <button>Send Review</button>
                                    </form>
                                     @endrole
                                     <div class="row">
                                         @foreach($comment as $commnt)
                                            @if($commnt->footballpitch ===$pitch->id)
                                         <div class="col-3 offset-1 card-comments-card">
                                               @foreach($user as $acc)
                                                @if($commnt->account === $acc->id)
                                              <h5> {{$acc->name}} {{$acc->surname}} </h5> 
                                                @endif
                                                @endforeach
                                                <h6>{{$commnt->comment}} </h6>
                                            @for($i=0 ; $i < $commnt->star;$i++)
                                                <span class="fa fa-star checked"></span>
                                              <!--  {{$commnt->star}}stella css label input-->
                                            @endfor
                                            @for($i=5;$i > $commnt->star;$i--)
                                            <span class="fa fa-star"></span>
                                            @endfor
                                         </div>
                                            @endif
                                         @endforeach
                                     </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
    </body>
    @include('layouts.footer')
<html>