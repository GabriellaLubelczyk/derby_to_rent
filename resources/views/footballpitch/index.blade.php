<html>
@extends('layouts.head')

    @include('layouts.header')

   <body>  
        <div class="container first-container">
            <div class="row intro-card">
                <div class="col-5">
                    <div> 
                        <button class="btn-filter" onclick="btnclick()" id="btn"><h1 class="intro-card-title"><i class="fab fa-airbnb"></i>Derby to Rent</h1></button>
                    </div>
                    <div id="filter" class=" ticket-card ">
                            <h2 class="ticket-card-title">Filter</h2>
                            <form method="HEAD" action="">
                                <label><h4>City:</h4></label>
                                <input class="form-control" type="text"name="city" placeholder="example Belfast" value="" >
                               <label><h4>Order By Price :</h4></label>
                                <select class="form-control" name="sort">
                                    <option value="asc">Price asc</option> 
                                    <option value="desc">Price desc</option> 
                                </select>
                               <!-- <label><h4>Date :</h4></label>
                                <input class="form-control" type="date"name="date" value="11/13/2019" >-->
                                <label><h4>Players :</h4></label>
                                <select class="form-control" name="player">
                                    <option value="22">22</option> 
                                    <option value="10">10</option> 
                                    <option value="8">8</option> 
                                </select>
                                <label><h4>Budget :</h4></label>
                                <input  class="form-control" name="budget" value="200"/>
                                <button id="btnSort" type="submit" class="buy-ticket"><h4>Search</h4></button>
                            </form>
                           <!-- <form>
                                <label><h4>City:</h4></label>
                                <input class="form-control" type="text" placeholder="example Belfast" > <br>
                                <label><h4>Check in:</h4></label>
                                <input class="form-control" type="date" value=""> <br>
                                <label><h4>Time:</h4></label>
                                <input class="form-control" type="time" value="">
                                <label><h4>For:</h4></label>
                                <select class="form-control">
                                    <option value="1">1 hour</option>
                                    <option value="2">2 hours</option>
                                    <option value="3">3 hours</option>
                                </select> <br>
                                <label><h4>Seats for playing:</h4></label>
                                <input class="form-control" type="text" value="22" />
                                <button class="buy-ticket"><h4>Search</h4></button>
                            </form>-->
                        </div>
                    </div>
                    
                <div class="col-6 offset-1">
                    <p class="intro-card-text">
                    Le origini della città affondano nella preistoria, nei dintorni vi sono, specialmente nel Peak District, numerosi siti neolitici come il "Nine Ladies" risalente a circa 35000 anni prima di Cristo. I romani costruirono il campo militare di Derventio sul precedente impianto celtico sulla riva orientale del fiume Derwent, probabilmente nella località detta Little Chester, mentre il sito della vecchia fortezza romana è a Chester Green. Più tardi la città fu uno dei "Five Boroughs" del Danelaw: Borough deriva dal termine anglo-sassone burg, che significa "città fortificata".
                    Djura-by, registrato in anglo-sassone come Deoraby cioè "Villaggio del Cervo" è una credenza popolare affermata da Tim Lambert, che scrive: "Il nome "Derby" deriva dalla parola danese "deor" che significa "insediamento del cervo". Non vi sono però prove per questa teoria.[1] Per altri è la corruzione dell'originale Derventio romano, a sua volta derivante dal celtico Derwentiu che significa "Corso d'acqua che fuoriesce da un querceto". Il nome della città appare, tuttavia, come Darby o Darbye sulle prime mappe moderne, come quella di Speed (1610).
                    La ricerca moderna (2004) ha dimostrato che i Vichinghi e gli anglosassoni probabilmente coabitarono, occupando due aree di terra circondate dall'acqua, se anche la "Cronaca anglosassone" (c. 900) dice che "Derby è divisa dall'acqua ". Queste aree di terreno erano conosciute come Norþworþig ("Northworthy" = "recinto nord") e Deoraby.[2]
                    </p>
                </div> 
            </div>
            @foreach ($pitch as $pitches)
                <div class="card football-card">
                    <div class="row">
                        <div class="col-6">
                            <h1 class="football-card-title">{{$pitches->name}}</h1>
                            <img class="football-card-img" src="{{$pitches->image}}">
                        </div>
                    <div class="col-6 football-card-text">
                        @php
                        $rating = DB::table('reviews')
                                            ->where('footballpitch' , $pitches->id)
                                            ->avg('star');
                        @endphp
                        <h3>Rating:
                         @for($i=0 ; $i < $rating;$i++)
                             <span class="fa fa-star checked"></span>
                         @endfor
                        @for($i=5;$i > $rating;$i--)
                            <span class="fa fa-star"></span>
                         @endfor
                        </h3>
                                            
                        <h5 class="football-card-city">City:{{$pitches->city}}</h5>
                        <h5>Address:{{$pitches->address}}</h5>
                        <h5>Postal-code:{{$pitches->postalcode}}</h5>
                        @foreach($user as $lind)
                            @if($lind->id === $pitches->landlord)	
                                <h5>Landlord: {{$lind->name}} {{$lind->surname}}</h5>
                            @endif
                        @endforeach
                        <h5>Max player:{{$pitches->max_player}} </h5>
                        <h5 class="ticket-card-price" >Price:{{$pitches->price_h}} £ at hour</h5>
                        <form action="{{route('footballpitch.show', $pitches->id)}}" method="GET">
                            <button type="submit" class="football-card-button">More details</button>
                        </form>
                        @auth
                        @if(($pitches->landlord === $id) OR ( auth()->user()->can('Admin')) )
                            <a class="action-link-edit btn btn-primary " href="{{route('footballpitch.edit',$pitches->id)}}">Edit pitch</a>
                            <form action="{{ route('footballpitch.destroy', $pitches->id)}}" method="post">
												@csrf
												@method('DELETE')
												<button class="action-link-delete btn btn-danger" action="submit">Delete
												</button>
										
										</form>
                        @endif
                        @endauth
                    </div>
                    </div>
                </div>
            @endforeach
            {{ $pitch->links() }}
        </div>
        @include('layouts.footer')
    </body>
    <script>
        var btn = document.getElementById("btn");
        var filter = document.getElementById("filter");
        var i = true;
        var a = 1;
        var b = 0;
        
        function btnclick() {
        if(document.getElementById('filter').style.display = 'none')
        {
            document.getElementById('filter').style.display = 'block';
            return
        }
        else
        {
            document.getElementById('filter').style.display = 'none';
            return
        }
    };
    </script>
</html>