@extends('layouts.head')
@include('layouts.header')

<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add an event</h1>
      <form method="post" action="{{ route('events.store') }}">
          @csrf
          <div class="form-group">    
              <label for="title">Title:</label>
              <input type="text" class="form-control" name="title"/>
          </div>

          <div class="form-group">
              <label for="image">Image:</label>
              <input type="text" class="form-control" name="image"/>
          </div>

          <div class="form-group">
              <label for="type_of_event">Date start:</label>
              <input type="date" class="form-control" name="start"/>
          </div>
          <div class="form-group">
              <label for="date">Date end:</label>
              <input type="date" class="form-control" name="end"/>
          </div>
          <div class="form-group">
              <label for="type_of_event"> Football pitch:</label>
              <select class="form-control" name="footballpitch_id" id="footballpitch_id">
                  @foreach($pitch as $camp)
                    <option value="{{$camp->id}}"> {{$camp->name}}</option>
                  @endforeach
              </select>
          </div>
          <div class="form-group">
              <label for="description">Decription:</label>
              <input type="text" class="form-control" name="description"/>
          </div>                         
          <button type="submit" class="btn btn-primary-outline">Add an event</button>
      </form>
  </div>
</div>
</div>
@include('layouts.footer')
