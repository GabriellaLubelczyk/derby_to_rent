@extends('base')
@section('main')
<html>
    <head>
        <h1> Show all events </h1>
        <a style="margin: 19px;" href="{{ route('events.create')}}" class="btn btn-primary">Return</a>

    </head>
    <div>
    <a style="margin: 19px;" href="{{ route('events.create')}}" class="btn btn-primary">New event</a>
    </div>  
    <div class="col-sm-12">
@section('main')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Event</h1>    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>Title</td>
          <td>Image</td>
          <td>Type of event</td>
          <td>Date</td>
          <td>Description</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>  <tbody>
        @foreach($events as $event)
        <tr>
            <td>{{$event->title}}</td>
            <td>{{$event->image}}</td>
            <td>{{$event->type_of_event}}</td>
            <td>{{$event->date}}</td>
            <td>{{$event->description}}</td>
            <td>
                <a href="{{ route('events.edit',$event->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('events.destroy', $event->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>  
  </table>
<div>
</div>
@endsection


  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</html>
