@extends('layouts.head')
@include('layouts.header')

<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Contact the landlord</h1>

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
      <form method="post" action="{{ route('contacts.store') }}">
          @csrf
          <div class="form-group">    
              <label for="name">Name:</label>
              <input type="string" class="form-control" name="name"/>
          </div>

          <div class="form-group">
              <label for="surname">Surname:</label>
              <input type="string" class="form-control" name="surname"/>
          </div>

          <div class="form-group">
              <label for="email">Email:</label>
              <input type="string" class="form-control" name="email"/>
          </div>
          <div class="form-group">
              <label for="message">Message:</label>
              <input type="text" class="form-control" name="message"/>
          </div>
            
          <button type="submit" class="btn btn-primary-outline">Send message</button>
      </form>
  </div>
</div>
</div>
@include('layouts.footer')
