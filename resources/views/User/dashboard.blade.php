<html>
@extends('layouts.head')

    @include('layouts.header')

   <body>     
   
        
        <div class="container">
            <div class="row">
                <div class="col-8">
                <div class="row">
                @role('isAdmin')
                <div class="col-12">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">City</th>
                            <th scope="col">Rating</th>
                            <th scope="col-3">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                                @foreach($pitch as $mypitch)
                          <tr>
                             
                                <th scope="row">{{$mypitch->id}}</th>
                                <td>{{$mypitch->name}}</td>
                                <td>{{$mypitch->city}}</td>
                                <td>
                                        @php
                                        $rating = DB::table('reviews')
                                                            ->where('footballpitch' , $mypitch->id)
                                                            ->avg('star');
                                        @endphp
                                        @for($i=0 ; $rating > $i;$i++)
                                         <span class="fa fa-star checked"></span>
                                        @endfor
                                        @for($i=$rating;$i < 5 ;$i++)
                                        <span class="fa fa-star"></span>
                                         @endfor
                                </td>
                                <td>
                                        <a class="btn btn-primary" href="{{route('footballpitch.edit',$mypitch->id)}}">Edit pitch</a>
                                </td>
                                <td>
                                        <form action="{{ route('footballpitch.destroy', $mypitch->id)}}" method="post">
												@csrf
												@method('DELETE')
												<button class="btn btn-danger" action="submit">Delete
												</button>
										
										</form>
                                </td>
                                <td>
                                        <a class="btn btn-success" href="{{route('footballpitch.show', $mypitch->id)}}">Show</a>
                                </td>
                               
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                      <form method="HEAD" action="">
                            <label>Name:</label>
                            <input  type="text"name="name" placeholder="example" value="" >
                            <button  type="submit">Search</button>
                        </form>
                </div>
                @endrole 
                @role('isLandlord')
                <div class="col-12">
                    <div class="calendar-card ">
                        <h1>Event's Calendar</h1>
                    @include('Item.fullcalendar')
                    </div>
                </div>
                <div class="col-12">
                        <h1>Your pitches:</h1>
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">City</th>
                            <th scope="col">Rating</th>
                            <th scope="col-3">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                                @foreach($pitch as $mypitch)
                          <tr>
                                @if($mypitch->landlord === ($user->id))
                                <th scope="row">{{$mypitch->id}}</th>
                                <td>{{$mypitch->name}}</td>
                                <td>{{$mypitch->city}}</td>
                                <td>
                                        @php
                                        $rating = DB::table('reviews')
                                                            ->where('footballpitch' , $mypitch->id)
                                                            ->avg('star');
                                        @endphp
                                        @for($i=0 ; $i < $rating;$i++)
                                         <span class="fa fa-star checked"></span>
                                        @endfor
                                        @for($i=5;$i > $rating;$i--)
                                        <span class="fa fa-star"></span>
                                         @endfor
                                </td>
                                <td>
                                        <a class="btn btn-primary" href="{{route('footballpitch.edit',$mypitch->id)}}">Edit pitch</a>
                                </td>
                                <td>
                                        <form action="{{ route('footballpitch.destroy', $mypitch->id)}}" method="post">
												@csrf
												@method('DELETE')
												<button class="btn btn-danger" action="submit">Delete
												</button>
										
										</form>
                                </td>
                                <td>
                                        <a class="btn btn-success" href="{{route('footballpitch.show', $mypitch->id)}}">Show</a>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                      <form method="HEAD" action="">
                            <label>Name:</label>
                            <input  type="text"name="name" placeholder="example" value="" >
                            <button  type="submit">Search</button>
                        </form>
                </div>
                <div class="col-12">
                        <h2>Top Sales :<h2>
                                <table class="table">
                                        <thead>
                                          <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Date</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Footballpitch</th>
                                            <th scope="col">Import</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                                @php 
                                                    $lol= 1;
                                                @endphp
                                                @foreach($ticket1 as $ticketz)
                                                @if($ticketz->status !== 1)
                                                    @if($ticketz->status == 2)
                                                    <tr class="ticket-accepted">
                                                    @else
                                                    <tr class="ticket-rejected">
                                                    @endif
                                                @endif
                                                            @foreach($pitch as $pic)
                                                            @if($pic->landlord === $user->id)
                                                            @if($pic->id === $ticketz->footballpitch)
                                                            <td>
                                                                @php 
                                                                    $lol =$lol + 1;
                                                                    echo $lol
                                                                @endphp
                                                            </td>
                                                            <td>
                                                                {{$ticketz->date}}
                                                            </td>
                                                            <td>
                                                                @foreach($account as $acc)
                                                                    @if($acc->id === $ticketz->account)
                                                                        {{$acc->name}} {{$acc->surname}}
                                                                    @endif
                                                                @endforeach
                                                            </td>
                                                            
                                                            <td>
                                                                        {{$pic->name}}
                                                            </td>
                                                            <td>
                                                            £ {{$ticketz->import}}
                                                                </td>
                                                            @endif
                                                            @endif
                                                            @endforeach
                                                    </tr>
                                                @endforeach
                                          
                                               
                                        
                                            
                                        </tbody>
                                      </table>
                            
                    </div>
                @endrole
                @role('isUser')
                <div class="col-12">

                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Date</th>
                            <th scope="col">Import</th>
                            <th scope="col">Footballpitch</th>
                            <th scope="col-3">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                                @foreach($ticket as $tickets)
                          <tr>
                               @if( $tickets->account === $user->id)
                                <th scope="row">{{ $tickets->id}}</th>
                                <td>{{ $tickets->date}}</td>
                                <td>{{ $tickets->import}}</td>
                                <td> 
                                    @foreach($pitch as $pitc)
                                        @if( $tickets->footballpitch === $pitc->id)
                                            {{$pitc->name}}
                                        @endif
                                    @endforeach
                                </td>
                                @php 
                                    $hello = $tick->id;
                                    $boi = $tick->id - $var;
                                    $boia = 1;
                                @endphp
                                @foreach($comment as $review)
                                        @if($tickets->id === $review->ticket)
                                            <td><a class="btn btn-primary" href="{{route('review.edit', $tickets->id)}}" >Edit Review</a> </td>
                                            <td><a class="btn btn-danger" href="{{route('review.delete', $review->id)}}" >Delete Review </a> </td>
                                        @else
                                        @php 
                                            $hello = $hello - 1 ;
                                        @endphp
                                        @endif
                                        @if( $hello == $boi )
                                        <td><a class="btn btn-secondary" href="{{route('review.createreview', $tickets->id)}}"  >Create Review </a> </td>
                                        @endif
                                @endforeach
                                    @if($var == 0 )
                                    <td><a class="btn btn-secondary" href="{{route('review.createreview', $tickets->id)}}"  >Create Review </a> </td>
                                    @endif
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                </div>
                @endrole
                @role('isLandlord')
                {{$x}}
                @if($x !== 0)
                <div class="col-12">
                    <h1>Amministration Tickets</h1>
                    <table class="table table-sm">
                            <thead>
                              <tr>
                                <th scope="col">#</th>
                                <th scope="col">Pitch</th>
                                <th scope="col">User</th>
                                <th scope="col">Player</th>
                                <th scope="col">Import</th>
                                <th scope="col-2">Action</th>

                              </tr>
                            </thead>
                            <tbody>
                                @foreach($bigliett as $tiko)
                                @foreach($pitch as $piaz)
                                @if($piaz->landlord === $user->id)
                                @if($piaz->id === $tiko->footballpitch)
                              <tr>
                                <th scope="row">{{$tiko->id}}</th>
                                <td> {{$piaz->name}}</td>
                                <td>
                                    @foreach($account as $acc)
                                        @if($acc->id === $tiko->account)
                                            {{$acc->name}} {{$acc->surname}}
                                        @endif
                                    @endforeach                        
                                </td>
                                <td>{{$tiko->number}}</td>
                                <td>£{{$tiko->import}} </td>
                                @if($tiko->status == 1)
                                <td>
                                    <a href="{{route('home.accept',$tiko->id)}}" class="btn btn-success">Accept<a>
                                    <a href="{{route('home.reject',$tiko->id)}}" class="btn btn-danger">Reject<a>
                                </td>
                                @endif
                              </tr>
                              @endif
                              @endif
                              @endforeach
                              @endforeach
                            </tbody>
                          </table>
                </div>
                @endif
                @endrole
            </div>
        </div>
        <div class="col-4">
            <div class="card-user ">
             
            <h3> Email: {{$user}}</h3>
            <h1> Your Account:</h1>
            @can('Create a pitch')
             <p class="text-permission" >You have permission to create new pitches <strong>A landlord</strong> is the owner of a house, apartment, condominium, land or real estate which is rented or leased to an individual or business, who is called a tenant (also a lessee or renter). When a juristic person is in this position, the term landlord is used. Other terms include lessor and owner. The term landlady may be used for female owners, and lessor may be used regardless of gender. The manager of a pub in the United Kingdom, strictly speaking a licensed victualler, is referred to as the landlord/lady.<p>
             <a class="add-pitch" href="{{route('footballpitch.create')}}"><i class="fas fa-plus">Add Pitch</i></a> <br>
             <a class="add-event" href="{{route('event.create')}}"><i class="fas fa-plus">Add Event</i></a>

         @endcan
            
         @can('Buy ticket')
             <p class="text-permission">You have permission to but Tickets <strong>A user </strong> is a person who utilizes a computer or network service. Users of computer systems and software products generally lack the technical expertise required to fully understand how they work.[1] Power users use advanced features of programs, though they are not necessarily capable of computer programming and system administration.</p>
         @endcan 
     </div>
         </div>
    </div>
            <div class="container">
                <div class="row">
                </div>
            </div>
        </div>
     @include('layouts.footer')
    </body>
</html>
