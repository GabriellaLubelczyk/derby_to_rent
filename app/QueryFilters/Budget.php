<?php


namespace App\QueryFilters;

class Budget extends Filter
{
    protected function applyFilter($builder)
    {
        return $builder->where('price_h','<=', request($this->filterName())) ;
    }
}