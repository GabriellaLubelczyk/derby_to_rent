<?php


namespace App\QueryFilters;

class Name extends Filter
{
    protected function applyFilter($builder)
    {
        return $builder->where('name', request($this->filterName())) ;
    }
}