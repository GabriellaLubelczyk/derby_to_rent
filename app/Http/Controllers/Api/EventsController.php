<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Event;
use Log;
use Illuminate\Support\Facades\DB;


class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $event = Event::all();
        return response()->json($event->toArray());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'=>'required',
            'image'=>'required',
            'start'=>'required',
            'end'=>'required',
            'description'=>'required'
        ]); 

        $event = new Event([
            'title' => $request->get('title'),
            'image' => $request->get('image'),
            'start' => $request->get('start'),
            'end' => $request->get('end'),
            'description' => $request->get('description'),
            'footballpitch_id'  => $request->get('footballpitch_id')
        ]);
        $event->save();

        return response()->json(['job' => 'success', 'event' => 'saved'], 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
       /* $event = Event::find($id);
        return response()->json($event->toArray());*/
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::find($id);
        $event->title =  $request->get('title');
        $event->image = $request->get('image');
        $event->footballpitch_id = $request->get('footballpitch_id');
        $event->start = $request->get('start');
        $event->end = $request->get('end');
        $event->description = $request->get('description');
        $event->save();

        return response()->json(['job' => 'success', 'Event' => 'updated'], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        $event->delete();
        return response()->json(['job' => 'success'], 200);
    }

    public function Calendar()
    {
        $event = DB::table('events')->get();
        return response()->json($event);
    }
}  