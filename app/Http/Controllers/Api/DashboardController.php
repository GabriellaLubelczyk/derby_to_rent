<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Log;
use Auth;
use App\footballpitch;
use App\User;
use App\Equipment;
use App\Ticket;
use App\Review;
use App\Event;
use App\QueryFilters\City;
use App\QueryFilters\Sort;
use App\QueryFilters\Player;
use App\QueryFilters\Budget;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Traits\HasRoles;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pitch = footballpitch::all()->count();
        $ticket = Ticket::all()->count();
        $user = User::all()->count();
        $purchase = 0;
        //purchase
        for ($i = 1; $i <= $ticket; $i++)
        {
            ${'ticke' . $i}= Ticket::find($i);
            $purchase = $purchase + ${'ticke' . $i}->import;
        }
        $array= ['pitch' => $pitch ,'ticket' =>  $ticket ,'user' =>  $user ,'purchase' =>  $purchase];
        return response()->json($array);
    }
    public function Tyear()
    {
        $data= array('' ,'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August','September', 'October','November','December');
        
        for ($i = 1; $i <= 12; $i++)
        {
            ${'ticke' . $i} = DB::table('tickets')->whereMonth('date',$i)->count();
            //${'mese' . $i} = $data[$i];
           // ${'mese' . $i} = $i;
        }
        //$array= [$mese1 => $ticke1 ,$mese2 => $ticke2 , $mese3 => $ticke3 ,$mese4 => $ticke4 ,$mese5 => $ticke5 ,$mese6 => $ticke6 ,$mese7 => $ticke7 ,$mese8 => $ticke8 ,$mese9 => $ticke9 ,$mese10 => $ticke10 ,$mese11 => $ticke11 ,$mese12 => $ticke12 ,];
        $array= [ $ticke1 , $ticke2 , $ticke3 , $ticke4 ,$ticke5 , $ticke6 , $ticke7 , $ticke8 , $ticke9 , $ticke10 , $ticke11 , $ticke12 ,];
        return response()->json($array);
    }

    public function Pyear()
    {
        $risultato = 0;
        $ciao = 0;
        /*for ($i = 1; $i <= 12; $i++)
        {
            $times = DB::table('tickets')->whereMonth('date',$i)->count();
            // ${'ticke' . $i} = DB::table('tickets')->whereMonth('date',$i)->select('import')->get();
            log::info($i);
           
        }*/
        for ($i = 1; $i <= 12; $i++)
        {
            ${'purch' . $i} = 0 ;
            ${'risultato' . $i} = 0;
            ${'ticke' . $i}=  DB::table('tickets')->whereMonth('date',$i)->get();
            foreach( ${'ticke' . $i} as $hello)
            {
                
                ${'purch' . $i} = ${'risultato' . $i} + $hello->import;
                ${'risultato' . $i} = ${'risultato' . $i} + $hello->import;
            }
        }
        //$array= [$mese1 => $ticke1 ,$mese2 => $ticke2 , $mese3 => $ticke3 ,$mese4 => $ticke4 ,$mese5 => $ticke5 ,$mese6 => $ticke6 ,$mese7 => $ticke7 ,$mese8 => $ticke8 ,$mese9 => $ticke9 ,$mese10 => $ticke10 ,$mese11 => $ticke11 ,$mese12 => $ticke12 ,];
        //$array= [ $ticke1 , $ticke2 , $ticke3 , $ticke4 ,$ticke5 , $ticke6 , $ticke7 , $ticke8 , $ticke9 , $ticke10 , $ticke11 , $ticke12 ,];
        $array= [ $purch1 ,$purch2, $purch3 ,$purch4 ,$purch5  ,$purch6 ,$purch7 ,$purch8 ,$purch9 ,$purch10 ,$purch11 ,$purch12];

        return response()->json($array);
    }
}
