<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Log;
use Auth;
use App\footballpitch;
use App\User;
use App\Equipment;
use App\Ticket;
use App\Review;
use App\Event;
use App\QueryFilters\City;
use App\QueryFilters\Sort;
use App\QueryFilters\Player;
use App\QueryFilters\Budget;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\DB;

class PitchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pitch = footballpitch::all();
        return response()->json($pitch->toArray());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|max:255',
        ]);
        $pitch = new footballpitch([
            'name'=> $request->get('name'),
            'image'=> $request->get('image'),
            'image2'=> $request->get('image2'),
            'address'=> $request->get('address'),
            'city'=> $request->get('city'),
            'price_h'=> $request->get('price_h'),
            'max_player'=> $request->get('max_player'),
            'postalcode'=> $request->get('postalcode'),
            'long'=> $request->get('long'),
            'lat'=> $request->get('lat')
        ]);
        $pitch->description = $request->get('description');
        $pitch->landlord = $request->get('landlord');
        $pitch->equipment = $request->get('equipment');
        $pitch->save();
        return response()->json(['job' => 'success', 'pitch' => 'saved'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pitch = footballpitch::find($id);
        return response()->json($pitch->toArray());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|max:255',
        ]);
        $pitch = footballpitch::find($id);
        $pitch->name =  $request->get('name');
        $pitch->image =  $request->get('image');
        $pitch->image2 =  $request->get('image2');
        $pitch->address =  $request->get('address');
        $pitch->city =  $request->get('city');
        $pitch->price_h =  $request->get('price_h');
        $pitch->max_player =  $request->get('max_player');
        $pitch->postalcode =  $request->get('postalcode');
        $pitch->long =  $request->get('long');
        $pitch->lat =  $request->get('lat');
        $pitch->description = $request->get('description');
        $pitch->landlord = $request->get('landlord');
        $pitch->equipment = $request->get('equipment');
        $pitch->save();
        return response()->json(['job' => 'success', 'pitch' => 'updated'], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pitch = footballpitch::find($id);
        $pitch->delete();
        return response()->json(['job' => 'success', 'pitch' => 'deleted'], 200);

    }
    public function AcceptPitch($id)
    {
        $pitch = footballpitch::find($id);
        if(   $pitch->status === 1 )
        {
            $pitch->status = 2 ; //accept
            $pitch->save();
        }
        return response()->json(['job' => 'success', 'pitch' => 'Accepted'], 200);
    }   
    public function RejectPitch($id)
    {
        $pitch = footballpitch::find($id);
        if(   $pitch->status === 1 )
        {
            $pitch->status = 3 ; //reject
            $pitch->save();
        }
        return response()->json(['job' => 'success', 'pitch' => 'rejected'], 200);
    } 
    public function top10ranking()
    {   
        $array = [];
        $ratings = [];
        $name = 'rating';
        $times = DB::table('footballpitches')->where('status' , 2)->count();
        $pitch = footballpitch::all()->where('status' , 2);
        for( $i = 1 ; $i <= $times ; $i ++)
        {
        $foots = DB::table('footballpitches')
        ->where('status' ,'=', 2)
        ->where('id' ,'=', $i)
        ->select('*')
        ->get();
        $foot = footballpitch::find($i)->where('status' , 2)->get();
        ${'rating' . $i} = DB::table('reviews')->where('footballpitch' , $i)->avg('star');
        if( ${'rating' . $i} === null)
        {
            ${'rating' . $i}= 0;

        }
        //array_push( $ratings,[ $name => ${'rating' . $i} ]);
     //   $foots->push([ $name => ${'rating' . $i} ] );
        array_push( $array,${'rating' . $i} );
       //$foot->rating = ${'rating' . $i};
       //array_push( $array, $foot , ${'rating' . $i} );
        }
        return response()->json($array);
    }

    public function ShowOrder()
    {
        $rating = DB::table('reviews')->join('footballpitches', 'reviews.footballpitch', '=', 'footballpitches.id')->groupBy('footballpitches.id')->avg('star');
        log::info( $rating);
        return response()->json($rating);
    }
    public function Mypitch($id)
    {
        $pitch = DB::table('footballpitches')->where( 'landlord' , '=' , $id )->get();
        return response()->json($pitch);
    }
    public function TicketBYPitch($id)
    {

        $ticket = DB::table('tickets')->join('footballpitches', 'tickets.footballpitch', '=', 'footballpitches.id')->where( 'landlord' , '=' , $id)->get();
        return response()->json($ticket);
    }
}
