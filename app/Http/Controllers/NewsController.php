<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use Log;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info("News index");

        $news = News::all();

        return view('news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Log::info("News create");

        return view('news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        log::info("News store");
        
        $request->validate([
            'title'=>'required',
            'image'=>'required',
            'type_of_news'=>'required',
            'date'=>'required',
            'description'=>'required'
        ]);

        $news = new News([
            'title' => $request->get('title'),
            'image' => $request->get('image'),
            'type_of_news' => $request->get('type_of_news'),
            'date' => $request->get('date'),
            'description' => $request->get('description')
        ]);
        $news->save();
        return redirect('/news')->with('success', 'News saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Log::info("News edit");

        $news = News::find($id);
        return view('news.edit', compact('news'));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Log::info("News update");

        $request->validate([
            'title'=>'required',
            'image'=>'required',
            'type_of_news'=>'required',
            'date'=>'required',
            'description'=>'required'
        ]);

        $news = News::find($id);
        $news->title =  $request->get('title');
        $news->image = $request->get('image');
        $news->type_of_news = $request->get('type_of_news');
        $news->date = $request->get('date');
        $news->description = $request->get('description');
        $news->save();

        return redirect('/news')->with('success', 'News updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Log::info("News destroy");

        $news = News::find($id);
        $news->delete();

        return redirect('/news')->with('success', 'News deleted!');
    }
}
