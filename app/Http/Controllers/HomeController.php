<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\footballpitch;
use App\Ticket;
use App\Review;
use App\Event;
use Auth;
use Log;
use App\News;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Pipeline\Pipeline;
use App\QueryFilters\Name;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $news = News::all();
        $event = Event::all();
        $userId = Auth::id();
        $bigliett = DB::table('tickets')->orderBy('date', 'desc')->where('status', 1)->get();
        $x = DB::table('tickets')->where([
            ['status', 1],
            ])->count();
        $user = User::find($userId);
        $pitch = footballpitch::query();
        $pitch = app(Pipeline::class)
        ->send(footballpitch::query())
        ->through([
            Name::class,
        ])
        ->thenReturn()
        ->get();
        $comment = Review::all();
        $ticket = DB::table('tickets')->orderBy('date', 'desc')->get();
        $count = 0 ;
        $account =  User::all();
        $ticket1 = DB::table('tickets')->orderBy('import', 'desc')->get();
        $tick = DB::table('tickets')->select('id')->orderBy('id', 'desc')->first();
        $var = DB::table('reviews')->count();
        return view('User.dashboard', compact('user','pitch','ticket','comment' ,'count','account', 'ticket1','tick','var','bigliett','x','news','event'));
    }
    public function AcceptTicket($id)
    {
        $ticket = Ticket::find($id);
        if(   $ticket->status === 1 )
        {
            $ticket->status = 2 ; //accept
            $ticket->save();
        }
        return redirect('/home');
    }   
    public function RejectTicket($id)
    {
        $ticket = Ticket::find($id);
        if(   $ticket->status === 1 )
        {
            $ticket->status = 3 ; //reject
            $ticket->save();
        }
        return redirect('/home');
    } 
    public function Setting($id)
    {
        $user = User::find($id);
        return view('User.settings',compact('user'));
    }
}
