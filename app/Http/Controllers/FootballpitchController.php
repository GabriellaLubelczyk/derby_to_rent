<?php

namespace App\Http\Controllers;

use Illuminate\Pipeline\Pipeline;
use Illuminate\Http\Request;
use Log;
use Auth;
use App\footballpitch;
use App\User;
use App\Equipment;
use App\Ticket;
use App\Review;
use App\Event;
use App\QueryFilters\City;
use App\QueryFilters\Sort;
use App\QueryFilters\Player;
use App\QueryFilters\Budget;
use Spatie\Permission\Traits\HasRoles;

class FootballpitchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        Log::info('index');
        $user = User::all();
        $event = footballpitch::find(1)->event;
        $equip = Equipment::all();
        $ticket = Ticket::query();
        $pitch = footballpitch::query();
        $pitch = app(Pipeline::class)
            ->send(footballpitch::query())
            ->through([
                City::class,
                Budget::class,
                Sort::class,
                Player::class,
            ])
            ->thenReturn()
            ->simplepaginate(4);
         /**$   if(request()->has('city')){
                $pitch->where('city', request('city'));
            }
            if(request()->has('sort')){
                $pitch->orderBy('price_h', request('sort'));
            }
            if(request()->has('date')){
                $ticket->where('date', request('date'));
            }
            if(request()->has('player')){
                $pitch->where('max_player', request('player')) ;
            }
        $pitch = $pitch->get();
        /**$pitch = $pitch->get();
        $ticket = $ticket->get();*/
        if(Auth::user())
        {
        $id = Auth::user()->id;
        return view('footballpitch.index', compact('pitch','user','equip','ticket','id','event'));
        }
            else
            { 
                $id = 0;
                 return view('footballpitch.index', compact('pitch','user','equip','ticket','id','event'));
                } 
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        if (Auth::check())
        {
        Log::info('create');
        return view('footballpitch.create');
        }
        else
        {
        abort('401');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info('edit');
        Log::info($request);
        
        $this->validate($request, [
            'name'=>'required|max:255',
        ]);
        $pitch = new footballpitch([
            'name'=> $request->get('name'),
            'image'=> $request->get('image'),
            'image2'=> $request->get('image2'),
            'address'=> $request->get('address'),
            'city'=> $request->get('city'),
            'price_h'=> $request->get('price_h'),
            'max_player'=> $request->get('max_player'),
            'postalcode'=> $request->get('postalcode'),
            'long'=> $request->get('long'),
            'lat'=> $request->get('lat')
        ]);
        $pitch->description = $request->get('description');
        $pitch->landlord = $request->get('landlord');
        $pitch->equipment = $request->get('equipment');
        Log::info($pitch);
        $pitch->save();
        return redirect('/');
   // $pitch->description =$request->get('description');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Log::info("Here");
        $pitch = footballpitch::find($id);
        $comment= Review::all();
        $user = User::all();
        $equip = Equipment::all();
        $ticket = Ticket::all();
        return view('footballpitch.show', compact('pitch','user','equip','comment','ticket'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        if (Auth::check())
        {
        Log::info('store');
        $pitch = footballpitch::find($id);
        return view('footballpitch.edit', compact('pitch'));
        }
        else
        {
        abort('401');
        }
        }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Log::info($request);
        $request->validate([
            'name'=>'required|max:255',
        ]);
        $pitch = footballpitch::find($id);
        $pitch->name =  $request->get('name');
        $pitch->image =  $request->get('image');
        $pitch->image2 =  $request->get('image2');
        $pitch->address =  $request->get('address');
        $pitch->city =  $request->get('city');
        $pitch->price_h =  $request->get('price_h');
        $pitch->max_player =  $request->get('max_player');
        $pitch->postalcode =  $request->get('postalcode');
        $pitch->long =  $request->get('long');
        $pitch->lat =  $request->get('lat');
        $pitch->description = $request->get('description');
        $pitch->landlord = $request->get('landlord');
        $pitch->equipment = $request->get('equipment');
        $pitch->save();
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::check())
        {
        log::info('elimina');
        $pitch1 = footballpitch::find($id);
      /*  $pitch1->dropForeignKey('landlord');
        $pitch1->dropForeignKey('equipment');*/
        $pitch1->delete();
        return redirect('/');}
        else
        {
        abort('401');
        }
        }
}
