<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'title',
        'image',
        'start',
        'end',
        'footballpitch_id',
        'description'
    ];
    public function footballpitch()
    {
        return $this->belongsTo('App\footballpitch');
    }
}
