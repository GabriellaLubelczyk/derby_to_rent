<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
        'date',
        'time',
        'import',
        'address',
        'number',
        'account',
        'footballpitch' 
    ];
    public function review()
    {
        return $this->hasOne('App\Review');
    }
}   
