<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFootballpitchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('footballpitches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name');
            $table->text('image');
            $table->text('image2')->nullable();
            $table->text('address');
            $table->text('city');
            $table->text('description');
            $table->integer('price_h');
            $table->integer('max_player')->default('22');
            $table->text('postalcode');
            $table->decimal('long', 10, 7);
            $table->decimal('lat', 10, 7);
            $table->integer('landlord')->nullable();
            $table->foreign('landlord')->references('id')->on('users')->onDelete('cascade');
            $table->integer('equipment')->nullable();
            $table->foreign('equipment')->references('id')->on('equipment')->onDelete('cascade');
            $table->integer('status')->default('1');
            $table->foreign('status')->references('id')->on('statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('footballpitches');
    }
}
