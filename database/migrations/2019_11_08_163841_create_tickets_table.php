<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->Increments('id');
            $table->timestamps();
            $table->date('date')->nullable();
            $table->time('time')->nullable();
            $table->float('import')->nullable();
            $table->integer('number')->nullable();
            $table->integer('footballpitch');
            $table->foreign('footballpitch')->references('id')->on('footballpitches')->onDelete('cascade');
            $table->integer('account');
            $table->foreign('account')->references('id')->on('users')->onDelete('cascade');
            $table->integer('status')->default('1');
            $table->foreign('status')->references('id')->on('statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
