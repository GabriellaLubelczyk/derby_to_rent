<?php

use Illuminate\Database\Seeder;

class FootballSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('footballpitches')->insert([
            'name'=> 'Ormeau Park Playing Fields',
            'image' => 'https://discovernorthernireland.com/contentassets/c97254231f184a0a90d772bfb9398411/ormeau-park5/?height=310&width=545&mode=crop',
            'image2'=> 'https://discovernorthernireland.com/contentassets/c97254231f184a0a90d772bfb9398411/ormeau-park/?height=310&width=545&mode=crop',
            'city'=> 'Belfast',
            'address'=> 'Ormeau Park Ormeau Road',
            'postalcode'=> 'BT7 3GG',
            'landlord'=> '1',
            'equipment'=> '1',
            'long'=> '-5.91641154853609',
            'lat'=> '54.590202243886'
        ]);
        DB::table('footballpitches')->insert([
            'name'=> 'Hammer Playing Fields',
            'image' => 'http://www.haffeysportsgrounds.co.uk/wp-content/uploads/2013/06/IMG_0620-650x400.jpg',
            'image2'=> 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRxBsHuPY8cwBz5fskvBKxqCMfLpfllxw-DA5YHUcdmLEvOKhD9&s',
            'city'=> 'Belfast',
            'address'=> 'Agnes Street Shankill Road',
            'postalcode'=> 'BT13 1GG',
            'landlord'=> '1',
            'equipment'=> '1',
            'long'=> '-5.94504066745723',
            'lat'=> '54.6061398477139'
        ]);
        DB::table('footballpitches')->insert([
            'name'=> 'Ulidia Playing Fields',
            'image' => 'https://pbs.twimg.com/media/D-QA7xyWsAUE5yQ.jpg',
            'image2'=> 'http://www.thenafl.co.uk/upload/grounds/1281447717Rosario%20pitch.jpg',
            'city'=> 'Belfast',
            'address'=> 'Ormeau Road',
            'postalcode'=> 'BT7 2GD',
            'landlord'=> '1',
            'equipment'=> '1',
            'long'=> '-5.91554971565789',
            'lat'=> '54.577527808237'
        ]);
    }
}
